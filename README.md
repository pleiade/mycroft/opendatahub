# Open Data Hub

## Quick Start

Prepare S3 buckets and aws-style secrets

```sh
oc project odh

oc apply -f bucket-opendatahub.yaml
./create-s3-secret.sh opendatahub

oc apply -f bucket-pachyderm.yaml
./create-s3-secret.sh pachyderm
```

Create KfDef

```sh
oc apply -f kfdef-opendatahub.yaml
```
