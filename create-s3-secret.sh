#!/bin/bash

prefix=${1:-opendatahub}

# oc create secret generic ${prefix}-s3 $(oc get secret/${prefix}-bucket -o=go-template='--from-literal access-id={{.data.AWS_ACCESS_KEY_ID|base64decode}} --from-literal access-secret={{.data.AWS_SECRET_ACCESS_KEY|base64decode}} --from-literal bucket={{(index .metadata.ownerReferences 0).name}}')

oc create secret generic ${prefix}-s3 \
   $(oc get secret/${prefix}-bucket -o=go-template='--from-literal access-id={{.data.AWS_ACCESS_KEY_ID|base64decode}} --from-literal access-secret={{.data.AWS_SECRET_ACCESS_KEY|base64decode}}') \
   $(oc get objectbucketclaim/${prefix}-bucket -o=go-template='--from-literal bucket={{.spec.bucketName}}')

oc get secret/${prefix}-s3 -o yaml
